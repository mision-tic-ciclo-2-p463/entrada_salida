import java.io.IOException;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {   
        int numBytes=0;
        char caracter;
        System.out.println("Por favor escriba un texto: ");
        try{
            do{
                caracter=(char) System.in.read();
                System.out.println(caracter);
                numBytes++;
            }while(caracter != '\n');
            System.err.printf("%d bytes leidos %n",numBytes);
        }
        catch(IOException e){
            System.err.println("error al capturar datos");
        }
    }
}
