import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class EntradaSalida {
    public static void main(String[] args) throws Exception {
        try {
            BufferedReader entrada =new BufferedReader(new InputStreamReader(System.in));             
            PrintWriter salida=new PrintWriter(System.out,true);
            salida.println("Por favor ingrese un texto: ");
            String mensaje = entrada.readLine();
            salida.println("el mensaje escrito es: "+mensaje);
        } catch (IOException e) {
            //TODO: handle exception
        } 
        
    }
    
}
