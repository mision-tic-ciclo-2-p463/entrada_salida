import java.io.IOException;

public class IOEstandar {
    public static void main(String[] args) throws Exception {
        //Crear buffer de bytes
        byte[] objBuffer=new byte[255];
        System.out.println("Por favor ingrese un texto: ");
        try{
            System.in.read(objBuffer,0,255);
            System.out.println("El texto escrito es: "+ new String(objBuffer));
        }
        catch(IOException e){
            
        }
    }
}
