import java.io.IOException;
import java.io.PrintWriter;

public class GestionArchivos {
    
    public static void main(String[] args) throws Exception {
        //int[][] numero=new int[5][5];
        int[][] numero = {{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15}};
        //Crear objeto para manipular salida de datos
        //String ruta_fichero="./numeros.txt";
        //PrintWriter salida=new PrintWriter("./numeros.txt");
        try (PrintWriter salida=new PrintWriter("./numeros.txt")){
            for(int i=0;i<numero.length;i++){
                for(int j=0;j<numero[i].length;j++){
                    salida.print(numero[i][j]+"\t");
                }
                salida.println("");
            }
        } catch (IOException e) {
            //TODO: handle exception
            System.err.println("Error al guardar datos");
        }
        
        //salida.close();
    }
}
